#!/bin/sh
export JBOSS_HOME=$(pwd)
export OFFSET=${jboss.offset}
export JAVA_OPTS="${jboss.javaopts}"

export JAVA_HOME=$JAVA8_HOME
export JAVA=$JAVA_HOME/bin/java

# start with an offset
$JBOSS_HOME/bin/standalone.sh -c standalone.xml -Djboss.socket.binding.port-offset=$OFFSET
